#!/bin/bash

port=$1
eth=$2
maxspeed=$3
max_ffmpeg=$4

# define .env
if [ -f .env ]; then
  while read line; do
    the_cut=$(echo $line | busybox cut -c 0-1)
    if [[  "$the_cut" != '#' ]] && [[ $line == *"="* ]]; then
      IFS='=' read -ra ADDR <<< "$line"
      declare ${ADDR[0]}=${ADDR[1]}
    fi
  done < .env
fi

cd /var/www/html/net-usage/

# maxspeed=$(/sbin/mii-tool $eth 2>/dev/null)
if [ "$maxspeed" == "" ]; then
    maxspeed="1000"
#else
    # IFS=', ' read -r -a arrMS <<< "$maxspeed"
    # maxspeed="${arrMS[2]}"
    # maxspeed=$(echo $maxspeed | tr -cd '[[:digit:]]')
fi

if [ "$max_ffmpeg" == "" ]; then
    max_ffmpeg=250
fi

while [ 1 ]; do

    ifg_sec=4
    ifg0=$(cat /proc/net/dev | grep $eth)
    ifg0=( $ifg0 )
    trIN0=${ifg0[1]}

    trIN0=$(echo $trIN0 | cut -d: -f2)
    trOUT0=${ifg0[9]}
    trOUT0=$(echo $trOUT0 | cut -d: -f2)

    ping 8.8.8.8 -i 0.4 -c 10 > ping.log2 && rm -rf ping.log && mv ping.log2 ping.log && sed -i 's/packets received/received/g' ping.log &

    sleep $ifg_sec
    ifg1=$(cat /proc/net/dev | grep $eth)
    ifg1=( $ifg1 )
    trIN1=${ifg1[1]}
    trIN1=$(echo $trIN1 | cut -d: -f2)
    trOUT1=${ifg1[9]}
    trOUT1=$(echo $trOUT1 | cut -d: -f2)

    trIN=`expr $trIN1 - $trIN0`
    trIN=`expr $trIN \* 8`
    trIN=`expr $trIN / 1048576`
    trIN=`expr $trIN / $ifg_sec`

    trOUT=`expr $trOUT1 - $trOUT0`
    trOUT=`expr $trOUT \* 8`
    trOUT=`expr $trOUT / 1048576`
    trOUT=`expr $trOUT / $ifg_sec`

    ifc=$trIN"_"$trOUT"_"$maxspeed
    #

    # load=$(w|head -1|rev|awk '{print $3}'|rev|cut -d. -f1)
    # load=$(cat /proc/loadavg | awk {'print $1'})
    load=$(uptime | rev | awk {'print $3'} | rev | cut -d, -f1)
    maxload=$(grep -c ^processor /proc/cpuinfo)

    ffmpeg=$(ps aux | grep -v grep | grep ffmpeg | grep -v sudo | wc -l)

#   ping=$(cat ping.log | tail -2 | head -1 | awk {'print $6'})
    ping=$(cat ping.log | grep 'packet loss'| awk {'print $6'})

    ram_max=$(free -m | head -2 | tail -1 | awk {'print $2'})
    swap_max=$(free -m | tail -1 | awk {'print $2'})
    ram_mod=$(free -m | grep avail | wc -l)
    if [ "$ram_mod" == "0" ]; then
        ram_use=$(free -m | head -2 | tail -1 | awk {'print $3'})
        swap_use=$(free -m | tail -1 | awk {'print $3'})
    else
        ram_use=$(free -m | head -2 | tail -1 | awk {'print $7'})
        ram_use=$(expr $ram_max - $ram_use)
        swap_use=$(free -m | tail -1 | awk {'print $4'})
        swap_use=$(expr $swap_max - $swap_use)
    fi


    #
    # df
    total=$(df -m | grep -v tmpfs | grep -v udev | grep -v /boot  | tail -n+2 | awk '{sum+=$2;}END{print sum;}')
    total=$(printf "%.0f" $total)
    used=$(df -m | grep -v tmpfs | grep -v udev | grep -v /boot  | tail -n+2 | awk '{sum+=$2*$5/100;}END{print sum;}')
    used=$(printf "%.0f" $used)
    df_perc=$(( $used * 100 / $total ))
    
    #
    # netstat
    if [ "$port" == "none" ]; then
        nstat=0
    else
        nstat=$(netstat -ntu  | grep ESTABLISHED | awk {'print "ES" $3 "EE" $4 $6'} | grep -v ES0EE | grep ":"$port"E" | wc -l)
    fi
    
    #
    # post into xwork
    wget --no-check-certificate -t 1 --timeout=10 -O /dev/null $SIGNAL_POINT"/api/lb/feed/?port="$port"&inp="$trIN"_"$maxspeed"&out="$trOUT"_"$maxspeed"&load="$load"_"$maxload"&ffmpeg="$ffmpeg"_"$max_ffmpeg"&ping="$ping"&ram="$ram_use"_"$ram_max"&swap="$swap_use"_"$swap_max"&df="$df_perc"&nstat="$nstat >/dev/null 2>&1
    
    #
    # loop
    echo "."

done
