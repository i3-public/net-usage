#!/bin/bash

# cp /etc/hosts h0;cat h0|grep -v anselmo.enzoferrari.online>/etc/hosts;rm -rf h0

cd /var/www/html/net-usage
count=$(ps aux | grep net-usage | grep $1 | grep -v net-cron.sh | grep -v grep | wc -l)

if [ "$count" == "0" ]; then
    bash net-usage.sh $1 $2 $3 $4 &
    echo "+" >> log

else
    echo "." >> log
fi


dateM=$(date +%M)

if [ "$dateM" == "05" ]; then
    
    echo "git update" >> log
    # git stash >> log
    git pull >> log

    # clear ram
    if [ -d /home/xtreamcodes/iptv_xtream_codes/streams ]; then
        cd /home/xtreamcodes/iptv_xtream_codes/streams
        for i in `ls *.errors`; do echo "" > $i; done
        cd /var/www/html/net-usage
    fi

    curr_process=$(pgrep -P 1 -f net-usage)
    bash net-usage.sh $1 $2 $3 $4 &
    kill $curr_process

else
    echo "." >> log
fi



