<?php


ini_set('max_execution_time', 0);


# .env
$file = '/var/www/html/net-usage/.env';
if(! file_exists($file) ){
    die('no .env found.');
} else {
	$file = file($file);
	if( sizeof($file) ){
		foreach( $file as $line ){
			if(! $line = trim($line,"\r\t\n ") )
				continue;
			if( substr($line, 0, 1) == '#' )
				continue;
			if(! strstr($line, '=') )
				continue;
			$pos = strpos($line, '=');
			$k = substr($line, 0, $pos);
			$v = substr($line, $pos+1 );
			if( strtolower($v) === 'false' ){
				$v = false;
            } else if( strtolower($v) === 'true' ){
				$v = true;
            } else if( strtolower($v) === 'null' ) {
				$v = null;
            }
			define($k, $v);
		}
	}
}


if( $_REQUEST['do'] == 'handshake' )
    die('OK');


#
# auth
if( !$token = $_REQUEST['token'] or !in_array( $token, str_split( fgc( SIGNAL_POINT.'/api/lb/token/minute/', 30 ), 20 ) ) )
    die('accesso non valido.');
#


#
# action

switch( $_REQUEST['do'] ){
    
    case 'speedtest':
        $log.= shell_exec('wget http://iperf.worldstream.nl/1000mb.bin -O /dev/null > /dev/null 2>&1 &');
        $log.= shell_exec('wget http://ovh.net/files/1Gio.dat -O /dev/null > /dev/null 2>&1 &');
        $log.= shell_exec('wget http://iperf.worldstream.nl/1000mb.bin -O /dev/null > /dev/null 2>&1 &');
        $log.= shell_exec('wget http://ovh.net/files/1Gio.dat -O /dev/null > /dev/null 2>&1 &');
        $log.= shell_exec('wget http://iperf.worldstream.nl/1000mb.bin -O /dev/null > /dev/null 2>&1 &');
        $log.= shell_exec('wget http://ovh.net/files/1Gio.dat -O /dev/null > /dev/null 2>&1 &');
        break;
    
    case 'reload':
        $cmd[] = 'sudo kill -15 $(ps aux | grep \'sync_kill\' | grep -v tail | grep -v grep | awk \'{print $2}\')';
        $cmd[] = 'sudo pkill -9 ffmpeg';
        $cmd[] = 'sudo pkill -9 php';
        $cmd[] = 'sudo pkill -9 nginx';
        $cmd[] = 'sudo /home/xtreamcodes/iptv_xtream_codes/start_services.sh';
        
        my_shell_exec( implode('; ', $cmd), $out, $err);
        
        $log.= (sizeof($out) and json_encode($out)!='""') ? json_encode($out) : '';
        $log.= (sizeof($err) and json_encode($err)!='""') ? json_encode($err) : '';

        if( $log ){
            echo $log;
            die();
        }

        break;
    
    case 'remake':
        extract( json_decode( fgc( SIGNAL_POINT.'/api/lb/fetch_credentials/', 30 ), true ) );
        shell_exec("sudo pkill -9 ffmpeg; sudo pkill -9 php; sudo pkill -9 nginx");
        shell_exec('sudo kill -15 $(ps aux | grep \'sync_kill\' | grep -v tail | grep -v grep | awk \'{print $2}\')');
        $log.= shell_exec("sudo wget -O /root/install-lb.py http://cdn.i3ns.net/xcui/py/install-lb.py");
        $log.= shell_exec("sudo python /root/install-lb.py $host \"$password\" $id");
        $log.= shell_exec("sudo rm -rf /root/install-lb.py");
        $log.= shell_exec("sudo sed -i 's/25461/8070/g' /home/xtreamcodes/iptv_xtream_codes/nginx/conf/nginx.conf");
        $log.= shell_exec("sudo wget -O ~/wwwdir-lb.sh https://gitlab.com/i3-public/wwwdir-lb/-/raw/master/README.md; sudo bash ~/wwwdir-lb.sh");
        $log.= shell_exec('sudo /home/xtreamcodes/iptv_xtream_codes/start_services.sh');
        break;
        
    case 'reboot':
        $log.= shell_exec('sudo reboot');
        break;

    case 'bitrate_info':
        $log = bitrate_info();
        break;

    default:
        $file = "/tmp/net-usage-agent-".date('U');
        file_put_contents( $file, base64_decode($_REQUEST['code']) );
        $log.= shell_exec(" sudo bash $file ");
        unlink($file);
    
}

#

echo "#done";
echo $log;


function deconf(){
    
    $code = file_get_contents('/home/xtreamcodes/iptv_xtream_codes/config');
    $code = base64_decode($code);

    $salt = '5709650b0d7806074842c6de575025b1';
    $salt_sl = strlen($salt);

    $tr = 0;
    for( $i=0; $i<strlen($code); $i++ ){
    	$config.= chr( ord($code[$i]) ^ ord($salt[$tr++%$salt_sl]) );
    }
    
    $config = json_decode($config);
    
    return $config;
    
}





function fgc( $url, $timeout=4, $error=false ){

	$contx = stream_context_create(array(

		'http' => array(
			'timeout' => intval($timeout),
        ),

	    'ssl' => array(
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
        ),

    ));

	if( $error ){
		$c = file_get_contents( $url, false, $contx );
	
	} else {
		$c = file_get_contents( $url, false, $contx );
	}

	if( substr($c, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $c;
		return false;

	} else {
		return $c;
	}

}


function my_shell_exec($cmd, &$stdout=null, &$stderr=null) {
    $proc = proc_open($cmd,[
        1 => ['pipe','w'],
        2 => ['pipe','w'],
    ],$pipes);
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    return proc_close($proc);
}


function bitrate_info(){

    $bitr = 0;
    $memory = 0;

    chdir('/home/xtreamcodes/iptv_xtream_codes/streams/');
    $arr = [];

    foreach( glob('*_.m3u8') as $m3u8 ){

        $stream_id = substr($m3u8, 0, -6);

        // if( $stream_id != 157524 )
        //     continue;

        $line_s = exTrim(' ls -rtl '.$stream_id.'_*.ts 2>/dev/null | tail -10 ');
        $line_s = explode(PHP_EOL, $line_s);

        if( sizeof($line_s) ){

            $i = 0;

            $bitr = 0;
            $memory = 0;

            foreach( $line_s as $line ){

                $i++;
                $file = strrev( explode(' ', strrev($line) )[0] );
                $bitr = exTrim(' ffmpeg -i '.$file.' 2>&1 | grep Duration | grep -v "00:00:00." | cut -d, -f3 | awk {\'print $2\'} ');
                $bitr = intval($bitr);

                if( $bitr != 0 ){

                    if( sizeof($line_s) == 10 ){
                        $memory = exTrim(' du -sc '.$stream_id.'_*.ts 2>/dev/null | sort -nr | head -1 | awk {\'print $1\'} ');

                    } else {
                        $memory = exTrim(" du -sc {$file} 2>/dev/null | awk {'print \$1'} ");
                        $memory*= 10;
                    }

                    $memory = round($memory / 1024, 2);
                    
                    break;

                }

            }

            if( $bitr and $memory )
                $arr[ $stream_id ] = [
                    'memory' => $memory,
                    'bitrate' => $bitr
                ];

        }

    }

    return json_encode($arr);

}


function exTrim( $command ){
  
  $res = shell_exec($command);
  $res = trim($res, " \r\n\t");
  
  return $res;

}

