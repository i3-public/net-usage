
# 
# wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s <port> <skipserver>

port=$1
skip=$2

if [ "$port" == "-" ]; then
    skip="skipserver"
fi

if [ "$skip" = "skipserver" ]; then
    
    if [ ! -d /var/www/html ]; then
        mkdir -p /var/www/html
    fi

    if [ -f /etc/centos-release ] || [ -f /etc/redhat-release ]; then
        yum update -y
        yum install -y git
    
    elif [ -f /etc/lsb-release ]; then
        apt-get update -y
        apt-get install -y git iproute2
    
    elif [ -f /etc/alpine-release ]; then
        apk update
        apk add git iproute2

    else
        echo "os not recognized"
        exit
    fi

    echo "skipping server installation .."

else

    if [ -f /etc/centos-release ] || [ -f /etc/redhat-release ]; then
        yum update -y
        yum install -y git httpd php7.4 libapache2-mod-php7.4
        sed -i 's/KeepAlive On/KeepAlive Off/g' /etc/httpd/httpd.conf
        sed -i 's/Listen 80/Listen 9320/g' /etc/httpd/ports.conf
        service httpd restart
        chkconfig httpd on
    
    elif [ -f /etc/lsb-release ]; then
        apt update -y
        apt -y install software-properties-common apt-transport-https
        add-apt-repository ppa:ondrej/php -y
        phpv="7."$(apt-cache search libapache2-mod-php7. | grep libapache2-mod-php7. | head -1 | awk {'print $1'} | cut -d. -f2)
        apt install -y git apache2 php$phpv libapache2-mod-php$phpv iproute2
        sed -i 's/KeepAlive On/KeepAlive Off/g' /etc/apache2/apache2.conf
        sed -i 's/Listen 80/Listen 9320/g' /etc/apache2/ports.conf
        service apache2 restart
    
    elif [ -f /etc/alpine-release ]; then
        apk update
        apk add apache2
        ln -s /var/www/localhost/htdocs /var/www/html

    else
        echo "os not recognized"
        exit
    fi

fi


# 
# git clone
cd /var/www/html
rm -rf net-usage
git clone https://gitlab.com/i3-public/net-usage.git


# 
# env
cp /var/www/html/net-usage/conf/.env_sample /var/www/html/net-usage/.env


# 
# sudo access
is_sudoer=$(cat /etc/sudoers | grep www-data | grep NOPASSWD| grep ALL|wc -l)
if [ $is_sudoer == 0 ]; then
    echo "www-data ALL=NOPASSWD: ALL" >> /etc/sudoers
fi


# 
# ifn
ifn=$(ip link show | head -3 | tail -1 | awk {'print $2'} | cut -d: -f1 | cut -d@ -f1)
if [ "$ifn" == "" ]; then
    ifn="eth0"
fi


# 
# speed
speed=$(cat /sys/class/net/$ifn/speed)

if [ "$speed" == "" ] || [ $speed == -1 ]; then
    speed=1000
fi

echo "the port is: "$port

# 
# cron
curr_cron=$(crontab -l)
crontab <<EOF
$curr_cron
* * * * * bash /var/www/html/net-usage/net-cron.sh $port $ifn $speed >/dev/null 2>&1
EOF


